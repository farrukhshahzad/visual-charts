# Visual Charts

Visual Charts is an interactive web-based tool to display various types of plots and charts of public data sets. 
It utilizes latest web technologies, including HTML5, JavaScript, Bootstarp and AngularJS, to visualize data on web browsers and smart devices. 


* [Web](http://www.visonics.net/visual-charts/)