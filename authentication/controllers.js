'use strict';
 
angular.module('Authentication')
 
.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        // reset login status
        AuthenticationService.ClearCredentials();
        $scope.error = "";
        $scope.login = function () {
            
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if (response.status==200) {
                    console.log("***", response);
                    AuthenticationService.SetCredentials($scope.username, $scope.password, response.data);
                    $location.path('/');
                } else {
                    console.log("---", response.data);
                    $scope.error = response.data["msg"];
                    $scope.dataLoading = false;
                }
            });
        };
        $scope.logout = function () {
            AuthenticationService.ClearCredentials();
        }
    }]);