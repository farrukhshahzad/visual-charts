<?php
 	require_once("Rest.inc.php");
	require_once('sendgrid-php.php');

	class API extends REST {
	
		public $data = "";
		static $account_id = 0;
		public $app = 12;
		public $appname = "Visual Parenting Tool";
		public $appweb = "http://www.visonics.net/visual-chart/parent.html";
		public $apiKey = "xx";

		private $db = NULL;
		private $mysqli = NULL;

		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection			
		}

		/*
		 *  Connect to Database
		*/
		private function dbConnect(){
			$this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
		}
				
		/*
		 * Dynmically call the method based on the query string
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404); // If the method not exist with in this class "Page not found".
		}

		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}	
		
		private function logoff() {
			static $account_id;
			$account_id = 0;
		}

		private function login(){
			static $account_id;

			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$user = json_decode(file_get_contents("php://input"),true);
			$account_id = 0;
			$email = $user['email'];		
			$password = $user['pwd'];
			//$username = $user['user'];
			if (!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$query="SELECT id, name, user, email,contact_person, contact_person2 FROM Account WHERE email = '$email' AND password = '".$password."' LIMIT 1";
					$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

					if($r->num_rows > 0) {
						$result = $r->fetch_assoc();	
						// If success everythig is good send header as "OK" and user details
						//error_log(implode(" ", $result));
						//error_log(phpversion());
						$account_id = $result['id'];
						//error_log($account_id);
						$this->response($this->json($result), 200);
					}
					$this->response('No user', 204);	// If no records "No Content" status
				}
			}
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}
        
        private function insertLogin($result){
            $columns = "user, name, ip, hostname, user_agent, status, app";
            $values = "'".$result['user']."','".$result['name']."','".$result['ip']."','".$result['hostname']."','".
                $result['user_agent']."','".$result['msg']."','".$this->app."'";
			$query = "INSERT INTO LoginHistory(".$columns.") VALUES(".$values.")";
            //error_log($query);
            $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
            $success = array('status' => "Success", "msg" => "Login History Created Successfully.", "data" => $result);
            return $success;
        }

		private function getData(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$query="SELECT distinct * FROM UserData order by id desc";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

			if($r->num_rows > 0){
				$result = array();
				while($row = $r->fetch_assoc()){
					$result[] = $row;
				}
				$this->response($this->json($result), 200); // send user details
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
        private function insertData() {
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$user = json_decode(file_get_contents("php://input"),true);
            $ip = $_SERVER['REMOTE_ADDR'];
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            $hostname = gethostbyaddr($ip); 

            $columns = "user, name, data, ip, hostname, user_agent, status, app";
            $values = "'".$user['email']."','".$user['name']."','".$user['data']."','".$ip."','".$hostname."','".
                $user_agent."','".$user['status']."','".$this->app."'";
			$query = "INSERT INTO UserData(".$columns.") VALUES(".$values.")";
            //error_log($query);
            $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
            $success = array('status' => "Success", "msg" => "User record Created Successfully.", "data" => $user);
			$this->response($this->json($success),200);
            return $success;
        }		
        
        private function contains($haystack, $needle)
        {
            return strpos($haystack, $needle) !== false;
        }

		private function loginUser(){

			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$user = json_decode(file_get_contents("php://input"),true);
			$username = $user['username'];
			$password = $user['password'];
            $ip = $_SERVER['REMOTE_ADDR'];
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            $hostname = gethostbyaddr($ip);            
			//error_log($username);
			$error = array('status' => "Failed", "msg" => "Invalid Data:400", 
                "hostname" => $hostname, "user_agent" => $user_agent);
            $error['user'] = $username;
            $error['name'] = $username;   
            $error['ip'] = $ip;  
			//$username = $user['user'];
			if (!empty($username) and !empty($password)){
                    $error['msg'] = "Invalid Username or Password";
                    $query="SELECT password FROM Account WHERE user = '".$username."' LIMIT 1";
                    $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
                    $crypt_pw = "--";
                    if($r->num_rows > 0) {
						$result = $r->fetch_assoc();
						$crypt_pw = $result['password'];
					}
                    $crypt_pw = crypt($password, $crypt_pw);
					$query="SELECT * FROM Account WHERE user = '".$username."' AND password = '".$crypt_pw."' LIMIT 1";
					//$query="SELECT * FROM Account WHERE user = '".$username."' AND password = '".$password."' LIMIT 1";
					//error_log($query);
					$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

					if($r->num_rows > 0) {
						$result = $r->fetch_assoc();
						unset($result['password']);
                        
						//error_log($ip, $hostname);
                        $result['ip'] = $ip;
                        $result['hostname'] = $hostname;
                        $result['user_agent'] = $user_agent;
                        $result['msg'] = "Success";
						//error_log(phpversion());
						//$account_id = $result['id'];
						//error_log($account_id);
                        if ((int)$result['status'] == 0)  {
                            $result['msg'] = "Access Denied!";
                            $this->insertLogin($result);
                            $this->response($this->json($result), 206);                            
                        }
                        $allowed_ips = $result['allowed_ips'];
                        unset($result['allowed_ips']);
                        $all = $this->contains($allowed_ips, ".,");
                        $exist = $this->contains($allowed_ips, $ip);
                        //if ($all) error_log("all true");
                        //if ($exist) error_log("exist true");
                        if (!($all or $exist))  {
                            $result['msg'] = "This IP has no Access!";
                            $this->insertLogin($result);
                            $this->response($this->json($result), 206);                            
                        }                        
                        $this->insertLogin($result);
						$this->response($this->json($result), 200);
					}
					//error_log("no user");
                    $this->insertLogin($error);
					$this->response($this->json($error), 206);	// If no records "No Content" status
			}
            $this->insertLogin($error);
			$this->response($this->json($error), 400);
		}
		
		private function customers(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$query="SELECT distinct * FROM Account order by id desc";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);

			if($r->num_rows > 0){
				$result = array();
				while($row = $r->fetch_assoc()){
					$result[] = $row;
				}
				$this->response($this->json($result), 200); // send user details
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
		private function customer(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){	
				$query="SELECT distinct * FROM Account where id=$id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				if($r->num_rows > 0) {
					$result = $r->fetch_assoc();	
					$this->response($this->json($result), 200); // send user details
				}
			}
			$this->response('',204);	// If no records "No Content" status
		}
		
		private function insertCustomer(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}

			$customer = json_decode(file_get_contents("php://input"),true);
			$column_names = array('name', 'description', 'email', 'city', 'address', 'state', 'zip', 
								  'web', 'phone', 'phone2', 'contact_person', 'contact_person2', 'logo', 'user', 'password', 'status', 'app');			
            $customer['status'] = 0;
            $customer['app'] = $this->app;
			$keys = array_keys($customer);
			$customer["password"] = crypt($customer["password"], $customer["email"]);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the customer received. If blank insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $customer[$desired_key];
				}
				$columns = $columns.$desired_key.',';
				$values = $values."'".$$desired_key."',";
			}
			$columns = $columns."creation_date,";
			$values = $values."CURRENT_TIMESTAMP(),";
			$query = "INSERT INTO Account(".trim($columns,',').") VALUES(".trim($values,',').")";
			if (!empty($customer)) {
				//error_log($query);  
				$r = $this->mysqli->query($query); //or die($this->mysqli->error.__LINE__);
				if ($r === false)  {
					//error_log($this->mysqli->error);
					$failed = array('status' => "Failed", "msg" => $this->mysqli->error, "data" => $customer);
					$this->response($this->json($failed), 206);
				}
				//Send email
				$an = $this->appname;
				$aw = $this->appweb;
				$un = $customer['user'];
				$to = addslashes(trim($customer['email']));
				$name = addslashes(trim($customer['name']));
				$from = 'info@visonics.net';
				$subject = "User Created - $an";

				$message = "Hi $name,<br><br>
				<h4>Welcome to $an!</h4><br><br>

				Please login to <a href='$aw'>$aw</a> to access the App.<br><br>
				Username: $un<br><br>

				Thank you for signing up!<br><br>

				<h4>Visonics Team</h4>
				";

				$from_email = new SendGrid\Email("Visonics", $from);
				$to_email = new SendGrid\Email($name, $to);
				$content = new SendGrid\Content("text/html", $message);

				$mail = new SendGrid\Mail($from_email, $subject, $to_email, $content);

				$bcc = new SendGrid\Email($an, $from);
    			$mail->personalization[0]->addTo($bcc);

				$sg = new \SendGrid($this->apiKey);
				$response = $sg->client->mail()->send()->post($mail);
				$code = (int)$response->statusCode();
				if ($code==202) {
					$email_sent = "Email sent to '$to'";
				}
				else {
					$email_sent = "Email Failed - $code";
				}

				$success = array('status' => "Success", "msg" => "User Created Successfully ($email_sent).", "data" => $customer);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	//"No Content" status
		}

		private function updateCustomer(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$customer = json_decode(file_get_contents("php://input"),true);
			$id = (int)$customer['id'];
			$column_names = array('name', 'description', 'email', 'city', 'address', 'state', 'zip', 
								  'web', 'phone', 'phone2', 'contact_person', 'contact_person2', 'logo');
			$keys = array_keys($customer['customer']);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the customer received. If key does not exist, insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $customer['customer'][$desired_key];
				}
				$columns = $columns.$desired_key."='".$$desired_key."',";
			}
			$columns = $columns."update_date=CURRENT_TIMESTAMP(),";
			$query = "UPDATE Account SET ".trim($columns,',')." WHERE id=$id";
			if(!empty($customer)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Customer ".$id." Updated Successfully.", "data" => $customer);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// "No Content" status
		}
		
		private function deleteCustomer(){
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			if($id > 0){				
				$query="DELETE FROM Account WHERE id = $id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Successfully deleted one record.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}

		private function updatePassword() {
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$customer = json_decode(file_get_contents("php://input"),true);
			//$id = (int)$customer['id'];
			$new_pass = $customer['new_pass'];
			$email = $customer["email"];
			$pass = crypt($new_pass, $email);
			$columns = "password='$pass', update_date=CURRENT_TIMESTAMP()";
			$query = "UPDATE Account SET ".$columns." WHERE email='$email'";
			//error_log($query);
			if ($email) {
				$r = $this->mysqli->query($query); //or die($this->mysqli->error.__LINE__);
				if ($r === false)  {
					$failed = array('status' => "Failed", "msg" => $this->mysqli->error, "data" => $email);
					$this->response($this->json($failed), 206);
				} else {
				    $success = array('status' => "Success", "msg" => "Password Updated Successfully.", "data" => $email);
				    $this->response($this->json($success),200);
				}
			} else
				$this->response('',204);	// "No Content" status
		}

		private function forgotPassword() {
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$customer = json_decode(file_get_contents("php://input"),true);
			$email = $customer['email'];

			$query = "SELECT name, user, email FROM Account WHERE email='$email'";
			//error_log($query);
	 	    $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
			if($r->num_rows > 0) {
				$result = $r->fetch_assoc();
				$name = $result['name'];
				$email = $result['email'];
				$user = $result['user'];
				$web = "http://" . $_SERVER['SERVER_NAME'];
                // Create a unique salt. This will never leave PHP unencrypted.
                $salt = "498#2D83B631%3800EBD!801600D*7E3CC13";
                // Create the unique user password reset key
                $password = hash('sha512', $salt.$email);

				//Send email
				$from = 'info@visonics.net';
				$subject = "Password Reset Request on ".$web;
				$an = $this->appname;
				$aw = $this->appweb;
				$to = addslashes(trim($email));
				$name = addslashes(trim($name));

                // Create a url which we will direct them to reset their password
				$pwrurl = $aw."#reset-password/$email/$password";

                $message = "Dear $name,<br><br>
                If this e-mail does not apply to you please ignore it. It appears that you have requested a password reset at our website $aw<br><br>
                To reset your password, please click the link below. If you cannot click it, please paste it into your web browser's address bar.<br><br>
                <a href='$pwrurl'>$pwrurl</a><br><br>
                Username: $user<br><br>
                Thanks,<br>Visonics Team
                ";

                //error_log($message);

				$from_email = new SendGrid\Email("Visonics", $from);
				$to_email = new SendGrid\Email($name, $to);
				$content = new SendGrid\Content("text/html", $message);

				$mail = new SendGrid\Mail($from_email, $subject, $to_email, $content);

				$sg = new \SendGrid($this->apiKey);
				$response = $sg->client->mail()->send()->post($mail);
				$code = (int)$response->statusCode();
				if ($code==202) {
					$email_sent = "Email sent to '$to";
					$success = array('status' => "Success", "msg" => "$subject Sent successfully ($email_sent).", "data" => $result);
				}
				else {
					$email_sent = "Email Failed - $code";
					$success = array('status' => "Failed", "msg" => "Reset Email Not sent! ($email_sent). Please try agian.", "data" => $result);
				}


				$this->response($this->json($success),200);

			} else {
			    $error = array("status" => "Failed", "msg" => "Email not registered!");
                $this->response($this->json($error), 206);
            }
		}

		private function resetPassword() {
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$email = $this->_request['email'];
			$reset = $this->_request['reset'];
			$salt = "498#2D83B631%3800EBD!801600D*7E3CC13";
			$resetkey = hash('sha512', $salt.$email);
            if ($resetkey == $reset) {

                $query = "SELECT name, user, email FROM Account  WHERE email='$email'";
                error_log($query);
                $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
                if($r->num_rows > 0) {
                    $result = $r->fetch_assoc();
                    $this->response($this->json($result), 200); // send user details
                }
			} else {
			    $error = array("status" => "Failed", "msg" => "Password reset key invalid!");
                $this->response($this->json($error), 206);
            }
		}

		private function deleteRecord(){
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			$table = $this->_request['table'];
			if($id > 0){				
				$query="DELETE FROM ".$table." WHERE id = ". $id;
				//error_log($query);
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Successfully deleted one record.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}

		private function controlRecord(){
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			$status = (int)$this->_request['status'];
			$table = $this->_request['table'];
			if($id > 0){				
				$query="Update ". $table ." set status=".$status." WHERE id = $id";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Successfully disabled one record.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}			

	}

	$api = new API;
	$api->processApi();

?>
