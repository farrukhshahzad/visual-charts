// General Parameters for this app, used during initialization
  var AllowTopLevel = false;
  var CellSize = new go.Size(68, 63);
   
    var green = '#228B22';
    var blue = '#81D4FA';
    var red = '#FA2021';
    var yellow = '#FFEB3B';
    var white = '#FFFFFF';

    var snow = '#FFFAFA';
    var lime = '#00FF00';
    var hotpink = '#FF69B4';
    var darkred= '#8B0000';
    var black= '#000000';

    var types =[{'color': white, 'text': 'Unknown', 'name': 'U', 'score': 0},
        {'color': green, 'text': 'Critical', 'name': 'C', 'score': 3},
        {'color': yellow, 'text': 'Important but not critical', 'name': 'I', 'score': 2},
        {'color': red, 'text': 'Forget/Ignore', 'name': 'F', 'score': 0},
        {'color': snow, 'text': 'Undecided or N/A', 'name': 'U', 'score': 0}, //4
        {'color': lime, 'text': 'Harmonious', 'name': 'H', 'score': 3}, //5
        {'color': hotpink, 'text': 'Friction', 'name': 'R', 'score': 1.5}, //6
        {'color': darkred, 'text': 'Conflict', 'name': 'X', 'score': -3}, //7
        {'color': black, 'text': 'Passive relationship', 'name': 'N', 'score': 3} //8
    ];

    var nodeDataArray = [];
    var nodeDataArray2 = [];
    var nodeDataArray3 = [];
    var nodeDataArray4 = [];
    var nodeDataArray5 = [];
    var nodeDataArray6 = [];
    var mapData = [];
    var dataDirty = true, allowUpdate = true;
    var bScore = 0, gScore = 0, pScore = 0;
    var nodeTemplate = null;
    var myDiagram, myDiagram2, myDiagram3, myDiagram4, myDiagram5, myDiagram6;

  function init_parents() {
    //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;

    myDiagram =
      $(go.Diagram, "myDiagramDiv",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position,
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          // For this sample, automatically show the state of the diagram's model on the page
          "ModelChanged": function(e) { 
              //process();
              if (e.isTransactionFinished) {
               // document.getElementById("savedModel").textContent = myDiagram.model.toJson();
              }
           },
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    // Regular Nodes represent items to be put onto racks.
    // Nodes are currently resizable, but if that is not desired, just set resizable to false.
    nodeTemplate =
      $(go.Node, "Auto",
        {
          resizable: false, resizeObjectName: "SHAPE", movable: false,
          // because the gridSnapCellSpot is Center, offset the Node's location
          locationSpot: new go.Spot(0, 0, CellSize.width / 2, CellSize.height / 2),
          cursor: ""
        },

        // this is the primary thing people see
        $(go.Shape, "Rectangle",
          { name: "SHAPE",
            fill: "white",
            minSize: CellSize,
            desiredSize: CellSize,  // initially 1x1 cell
            click: function (e, obj) {toggleNode(e, obj); },
            toolTip:  // define a tooltip for each node that displays the color as text
                    $(go.Adornment, "Auto",
                        $(go.Shape, { fill: "#FFFFCC" }),
                        $(go.TextBlock, { margin: 2 },
                            new go.Binding("text", "text"))
                    )  // end of Adornment             
          },
          new go.Binding("fill", "color"),
          new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)),
        // with the textual key in the middle
        $(go.TextBlock,
        { 
              alignment: go.Spot.Center, font: 'bold 14px sans-serif',
              stroke: "#888888",
              click: function (e, obj) {toggleNode(e, obj); }      
        },
          new go.Binding("text", "name"),
          new go.Binding("stroke", "stroke"))
      );  // end Node
      
       myDiagram.nodeTemplate = nodeTemplate;  
     // create an array of data describing nodes

    for (var i = 0; i < 30; i++) {
      var data = {
        key: i.toString(),
        type: 0,
        name: "U",
        text: "Unknwon",
        color: white,
        size: CellSize
      }; 
      nodeDataArray.push(data);
    }

    layout(myDiagram, nodeDataArray);

  }

  function init_children() {
    var $ = go.GraphObject.make;  
    myDiagram2 =
      $(go.Diagram, "myDiagramDiv2",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position, 
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    myDiagram3 =
      $(go.Diagram, "myDiagramDiv3",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position, 
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    myDiagram2.nodeTemplate = nodeTemplate;
    myDiagram3.nodeTemplate = nodeTemplate;
        
    for (var i = 0; i < 30; i++) {
      var data = {
        key: i.toString(),
        type: 0,
        name: "U",
        text: "Unknwon",
        color: white,
        size: CellSize
      }; 
      nodeDataArray2.push(data);
    }
    for (var i = 0; i < 30; i++) {
      var data = {
        key: i.toString(),
        type: 0,
        name: "U",
        text: "Unknwon",
        color: white,
        size: CellSize
      }; 
      nodeDataArray3.push(data);
    }     

    layout(myDiagram2, nodeDataArray2);
    layout(myDiagram3, nodeDataArray3);        
  }

  function init_family() {
    var $ = go.GraphObject.make;
    myDiagram4 =
      $(go.Diagram, "myDiagramDiv4",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position, 
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    myDiagram5 =
      $(go.Diagram, "myDiagramDiv5",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position, 
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    myDiagram6 =
      $(go.Diagram, "myDiagramDiv6",
        {
          layout: $(go.GridLayout,
                    { wrappingColumn: 5, alignment: go.GridLayout.Position, 
                        sorting: go.GridLayout.Forward, spacing: new go.Size(2, 2) }),
          allowCopy: false, allowDelete: false, 
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });

    myDiagram4.nodeTemplate = nodeTemplate;
    myDiagram5.nodeTemplate = nodeTemplate;
    myDiagram6.nodeTemplate = nodeTemplate;
    process();

  }

function init()
{
    init_parents();
    init_children();
    init_family();  
    setCursor();  
}


function toggleNode(e, obj ) { 
        var typ = obj.part.data.type;
        var key = obj.part.data.key;
        nodeDataArray = e.diagram.model.nodeDataArray;

        console.log('key', key, typ, nodeDataArray[key]); 
        if (typ<4 && allowUpdate) {           
                    typ++;
                    if (typ>=4) typ = 0;

                    nodeDataArray[key].type = typ;
                    nodeDataArray[key].name = types[typ].name;
                    nodeDataArray[key].text = types[typ].text;
                    nodeDataArray[key].color = types[typ].color;
                    
                    layout(e.diagram, nodeDataArray);
                    dataDirty = true;
                    $('.nav-tabs a[data-target="#map"]').hide();
                    process();  
      }
        
}

    // create a Model that does not know about link or group relationships
    // Update the layout from the controls, and then perform the layout again
function layout(diagram, nodeDataArray) {
    diagram.clear();
    diagram.model = new go.Model(nodeDataArray);
    diagram.startTransaction("change Layout");
    var lay = diagram.layout;
    diagram.commitTransaction("change Layout");    
}

function process()
{
    nodeDataArray4.length = 0;
    nodeDataArray5.length = 0;
    nodeDataArray6.length = 0;
    mapData.length = 0;

    pData = myDiagram.model.nodeDataArray;
    bData = myDiagram2.model.nodeDataArray;// || nodeDataArray2;
    gData = myDiagram3.model.nodeDataArray; // || nodeDataArray3;
    bScore = 0, gScore = 0, pScore = 0;

    for (var i = 0; i < 30; i++) {
        var ptype = pData[i].type; 
        var mapType = {'parent': ptype };       
        var btype = 1;

        if (i<4 || i in {5:'', 8:'', 10:'', 15:'', 20:'', 21:'', 25:'', 26:''})
          btype = 2;
        //parent map
        var type = 4;
        if ((ptype==1 && btype==1) || (ptype==2 && btype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && btype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && btype==3) || (ptype==3 && btype==1))
            type = 7;             
        else if ((ptype==2 && btype==3) || (ptype==3 && btype==2))
            type = 7;             
        else if (ptype==3 && btype==3) 
            type = 8; 
        if (type>4)
           console.log(type);
        var data = {
            key: i.toString(),
            type: type,
            name: types[type].name,
            text: types[type].text,
            color: types[type].color,
            size: CellSize
        };  
        nodeDataArray6.push(data);
        mapType['family-parent'] =  type;
        pScore += types[type].score;

        var btype = bData[i].type;
        var gtype = gData[i].type;
        mapType['boys'] =  btype;
        mapType['girls'] =  gtype;
        
        if (ptype>0)
           console.log(i, ptype, btype, gtype);

        //boys   
        var type = 4;
        if ((ptype==1 && btype==1) || (ptype==2 && btype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && btype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && btype==3) || (ptype==3 && btype==1))
            type = 7;             
        else if ((ptype==2 && btype==3) || (ptype==3 && btype==2))
            type = 7;             
        else if (ptype==3 && btype==3) 
            type = 8; 
        if (type>4)
           console.log(type);
        var data = {
            key: i.toString(),
            type: type,
            name: types[type].name,
            text: types[type].text,
            color: types[type].color,
            size: CellSize
        };  
        nodeDataArray4.push(data);
        mapType['family-boys'] =  type;
        bScore += types[type].score;

        //girls
        var type = 4;
        if ((ptype==1 && gtype==1) || (ptype==2 && gtype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && gtype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && gtype==3) || (ptype==3 && gtype==1))
            type = 7;             
        else if ((ptype==2 && gtype==3) || (ptype==3 && gtype==2))
            type = 7;             
        else if (ptype==3 && gtype==3) 
            type = 8; 
        if (type>4)
           console.log(type);
        var data = {
            key: i.toString(),
            type: type,
            name: types[type].name,
            text: types[type].text,
            color: types[type].color,
            size: CellSize
        }; 
        nodeDataArray5.push(data);     
        mapType['family-girls'] =  type;
        gScore += types[type].score;

        mapData.push(mapType);
    };
    pScore = Math.round((pScore/90) * 100);
    bScore = Math.round((bScore/90) * 100);
    gScore = Math.round((gScore/90) * 100);

    layout(myDiagram4, nodeDataArray4);
    layout(myDiagram5, nodeDataArray5);
    layout(myDiagram6, nodeDataArray6);
}

function setCursor() {
  myDiagram.setProperties({"defaultCursor": (allowUpdate ? 'pointer' : 'not-allowed')});
  myDiagram2.setProperties({"defaultCursor": (allowUpdate ? 'pointer' : 'not-allowed')});
  myDiagram3.setProperties({"defaultCursor": (allowUpdate ? 'pointer' : 'not-allowed')});
  myDiagram4.setProperties({"defaultCursor": 'not-allowed', "allowSelect": false});
  myDiagram5.setProperties({"defaultCursor": 'not-allowed', "allowSelect": false});
  myDiagram6.setProperties({"defaultCursor": 'not-allowed', "allowSelect": false});
}