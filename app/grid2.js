// General Parameters for this app, used during initialization
  //var AllowTopLevel = false;
  //var CellSize = new go.Size(68, 63);
   
    var green = '#228B22';
    var blue = '#81D4FA';
    var red = '#FA2021';
    var yellow = '#FFEB3B';
    var white = '#FFFFFF';

    var snow = '#FFFAFA';
    var lime = '#00FF00';
    var hotpink = '#FF69B4';
    var darkred= '#8B0000';
    var black= '#000000';

    var types =[{'color': 'white', 'text': 'Unknown', 'name': 'U', 'score': 0},
        {'color': 'green', 'text': 'Critical', 'name': 'C', 'score': 3},
        {'color': 'yellow', 'text': 'Important but not critical', 'name': 'I', 'score': 2},
        {'color': 'red', 'text': 'Forget/Ignore', 'name': 'F', 'score': 0},
        {'color': 'snow', 'text': 'Undecided or N/A', 'name': 'U', 'score': 0}, //4
        {'color': 'lime', 'text': 'Harmonious', 'name': 'H', 'score': 3}, //5
        {'color': 'hotpink', 'text': 'Friction', 'name': 'R', 'score': 1.5}, //6
        {'color': 'darkred', 'text': 'Conflict', 'name': 'X', 'score': -3}, //7
        {'color': 'black', 'text': 'Passive relationship', 'name': 'N', 'score': 3} //8
    ];

    var mapData = [];
    var dataDirty = true, allowUpdate = true;
    var bScore = 0, gScore = 0, pScore = 0;


function init()
{
    for (var i = 0; i < 30; i++) 
        mapData[i] = {'parent': 0, 'children-boys': 0, 'children-girls': 0, 'parent-score': 4, 'boys-score': 4, 'girls-score': 4};
}

function pToggle(domObj, index, map) {
   console.log(domObj.classList.item(0), index, map);
   var type = 0;

   if (!allowUpdate) return;

   if (domObj.classList.item(0)==="white")
        { type = 1; domObj.classList.remove("white"); }
   else if (domObj.classList.item(0)==="green")
        {type = 2; domObj.classList.remove("green");}
   else if (domObj.classList.item(0)==="yellow")
        {type = 3; domObj.classList.remove("yellow");}
   else if (domObj.classList.item(0)==="red")
        {type = 0; domObj.classList.remove("red");}

   console.log(domObj.classList.item(0), index, map, type);

   domObj.innerHTML = types[type].name;  
   domObj.title = types[type].text;   
   domObj.classList.add(types[type].color);
   mapData[index][map] = type;  
}

function process()
{
    bScore = 0, gScore = 0, pScore = 0;

    for (var i = 0; i < 30; i++) {
        var ptype = mapData[i]['parent']; 
        var btype = 1;

        if (i<4 || i in {5:'', 8:'', 10:'', 15:'', 20:'', 21:'', 25:'', 26:''})
          btype = 2;
        //parent map
        var type = 4;

        if ((ptype==1 && btype==1) || (ptype==2 && btype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && btype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && btype==3) || (ptype==3 && btype==1))
            type = 7;             
        else if ((ptype==2 && btype==3) || (ptype==3 && btype==2))
            type = 7;             
        else if (ptype==3 && btype==3) 
            type = 8; 

        
        mapData[i]['parent-score'] = type;     
        domObj = document.getElementById('p'+i.toString());  

        console.log(ptype, type, 'p'+i.toString());

        domObj.innerHTML = types[type].name;  
        domObj.title = types[type].text;
        domObj.classList.remove(domObj.classList.item(0));   
        domObj.classList.add(types[type].color);      
        pScore += types[type].score;

        var btype = mapData[i]['children-boys'];
        var gtype = mapData[i]['children-girls'];
        
        console.log(i, ptype, btype, gtype);

        //boys   
        var type = 4;
        if ((ptype==1 && btype==1) || (ptype==2 && btype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && btype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && btype==3) || (ptype==3 && btype==1))
            type = 7;             
        else if ((ptype==2 && btype==3) || (ptype==3 && btype==2))
            type = 7;             
        else if (ptype==3 && btype==3) 
            type = 8; 


        mapData[i]['boys-score'] = type;     
        domObj = document.getElementById('b'+i.toString());  
        domObj.innerHTML = types[type].name;  
        domObj.title = types[type].text;
        domObj.classList.remove(domObj.classList.item(0));    
        domObj.classList.add(types[type].color);      
        bScore += types[type].score;

        if (type>4)
           console.log("boys", btype, type, 'b'+i.toString());

        //girls
        var type = 4;
        if ((ptype==1 && gtype==1) || (ptype==2 && gtype==2))
            type = 5;        
        else if (ptype in {2:'', 1:''} && gtype in {2:'', 1:''})
            type = 6;
        else if ((ptype==1 && gtype==3) || (ptype==3 && gtype==1))
            type = 7;             
        else if ((ptype==2 && gtype==3) || (ptype==3 && gtype==2))
            type = 7;             
        else if (ptype==3 && gtype==3) 
            type = 8; 

        mapData[i]['girls-score'] = type;   
        domObj = document.getElementById('g'+i.toString());  
        domObj.innerHTML = types[type].name;  
        domObj.title = types[type].text;   
        domObj.classList.remove(domObj.classList.item(0)); 
        domObj.classList.add(types[type].color);      

        gScore += types[type].score;
        if (type>4)
           console.log("girls", gtype, type, 'g'+i.toString());
    };
    pScore = Math.round((pScore/90) * 100);
    bScore = Math.round((bScore/90) * 100);
    gScore = Math.round((gScore/90) * 100);

}

function setMap() {
   //console.log(domObj.classList.item(0), index, map);
   var map2 = ["parent", "children-boys", "children-girls", "parent-score", "boys-score", "girls-score"];
   var map = ["parent", "boys", "girls", "family-parent", "family-boys", "family-girls"];
   //map_id = ["p", "b", "g", "ps", ""]
   for (var j = 0; j < 6; j++) {
	   for (var i = 0; i < 30; i++) {
	       var type = mapData[i][map[j]];
 
		   domObj = document.getElementById(map[j]+i.toString());
		   if (type==undefined) {
			   type = mapData[i][map2[j]];
			   //console.log(i, j, type);
			   //if (types[type]==undefined) continue;
			}
			
		   domObj.innerHTML = types[type].name;  
		   domObj.title = types[type].text;   
		   domObj.classList.remove(domObj.classList.item(0)); 
		   domObj.classList.add(types[type].color);
	   }
   }
}
