'use strict';

angular.module('Authentication', []);
//var app = angular.module('myApp', ['ngRoute']);

var app = angular.module('myApp', [
    'Authentication',
    'ngRoute',
    'ngCookies'
]);

var logged_status = null;
var logged_id = 0;

app.factory("services", ['$http', '$cookieStore', '$rootScope',
    function($http, $cookieStore, $rootScope) {
        var serviceBase = 'http://visonics.net/visual-chart/services/';
        var obj = {};

        obj.getAccountId = function () {
            logged_id = 0
            if ($rootScope.globals.currentUser && $rootScope.globals.currentUser.user)
                logged_id = $rootScope.globals.currentUser.user.id;
            return logged_id
        };

        obj.getAccountInfo = function () {
            var user = null;
            if ($rootScope.globals.currentUser && $rootScope.globals.currentUser.user)
                user = $rootScope.globals.currentUser.user;
            return user
        };

        obj.login = function (user) {
            return $http.post(serviceBase + 'login', user).then(function (results) {
                logged_id = 0;
                if (results.status==200)
                    logged_id = results.data['id'];
                localStorage.setItem("logged_id", logged_id);
                console.log("***", obj.getAccountInfo(), results.data);
                return results.data || null;
            });
        };

        obj.getCustomers = function(){
            return $http.get(serviceBase + 'customers');
        };
        obj.getCustomer = function(customerID){
            return $http.get(serviceBase + 'customer?id=' + customerID);
        };

        obj.insertCustomer = function (customer, callback) {
            return $http.post(serviceBase + 'insertCustomer', customer).then(function (results) {
                callback(results);
                return results;
            });
        };

        obj.updateCustomer = function (id, customer) {
            return $http.post(serviceBase + 'updateCustomer', {id:id, customer:customer}).then(function (status) {
                return status.data;
            });
        };

        obj.deleteCustomer = function (id) {
            return $http.delete(serviceBase + 'deleteCustomer?id=' + id).then(function (status) {
                return status.data;
            });
        };

        obj.updatePassword = function (id, new_pass, user, callback) {
            return $http.post(serviceBase + 'updatePassword', {id:id, new_pass: new_pass, email: user['email'] }).
            then(function (results) {
                callback(results);
                return results;
            });
        };
        obj.forgotPassword = function (email, callback) {
            return $http.post(serviceBase + 'forgotPassword', {email: email }).
            then(function (results) {
                callback(results);
                return results;
            });
        };

        obj.resetPassword = function (email, reset) {
            return $http.get(serviceBase + 'resetPassword?email='+ email + '&reset=' + reset);
        };

		obj.getData = function(){
            return $http.get(serviceBase + 'getData');
        };
        obj.insertUser = function (customer, callback) {
            return $http.post(serviceBase + 'insertData', customer).then(function (results) {
                callback(results);
                return results;
            });
        };

        obj.insertFile = function (account_id, id, data) {
            return $http.post(serviceBase + 'insertFile', {account_id:account_id, id: id, name:data[0], data:data[1]}).then(function (results) {
                //callback(results);
                return results;
            });
        };

        obj.getFiles = function(){

            return $http.get(serviceBase + 'files?account=' + obj.getAccountId());
        };

        obj.getFile = function(id){
            return $http.get(serviceBase + 'get_file?id=' + id);
        };

        obj.deleteFile = function (id) {
            return $http.delete(serviceBase + 'deleteFile?id=' + id).then(function (status) {
                return status.data;
            });
        };

        return obj;
    }]);

app.directive("compareTo", function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
});

app.controller('passwordCtrl', function ($scope, $rootScope, $location, $routeParams, services, user) {

    var customerID = services.getAccountId();
    $scope.error = "";
    $scope.title = "Change Password";
    var page = $location.path().split('/')[1];
    if (page=="reset-password") $scope.title = "Reset Password";

    $scope.show_form = true;
    if (user) {
        $scope.user = user.data;
        if ($scope.user.msg) { $scope.error = $scope.user.msg; $scope.show_form = false;}
    } else
        $scope.user = services.getAccountInfo();

    //console.log(customerID, $scope.user);
    $scope._id = customerID;

    $scope.email = "";
    $scope.new_password = "";
    $scope.password = "";
    $scope.dataLoading = false;

    $scope.changePassword = function() {
        //$location.path('/');
        console.log(customerID, $scope.new_password);
        $scope.dataLoading = true;
        services.updatePassword(customerID, $scope.new_password, $scope.user,  function(response) {
                if (response.status==204)
                    $scope.error = "Password change Failed!";
                else {
                    $scope.error = response.data["msg"];
                    $scope.show_form = false;
                }
            }
        );
        $scope.dataLoading = false;
        console.log($scope.error);
    };

    $scope.forgotPassword = function() {
        //$location.path('/');
        console.log( $scope.email);
        $scope.dataLoading = true;
        services.forgotPassword($scope.email, function(response) {
                console.log( response);
                $scope.error = response.data["msg"];
            }
        );
        $scope.dataLoading = false;
        console.log($scope.error);
    };
});


app.controller('listCtrl', function ($scope, services) {
    services.getCustomers().then(function(data){
        $scope.customers = data.data;
    });
});

app.controller('editCtrl', function ($scope, $rootScope, $location, $routeParams, services, customer) {
    var customerID = services.getAccountId(); //($routeParams.customerID) ? parseInt($routeParams.customerID) : 0;
    console.log(customerID);
    $rootScope.title = (customerID > 0) ? 'Edit User' : 'Add User';
    $scope.buttonText = (customerID > 0) ? 'Update User' : 'Add New User';
    var original = customer.data;
    //original._id = customerID;
    $scope.customer = angular.copy(original);
    $scope._id = customerID;
    $scope.error = "";
    $scope.dataLoading = false;

    $scope.isClean = function() {
        return angular.equals(original, $scope.customer);
    };

    $scope.deleteCustomer = function(customer) {
        $location.path('/');
        if(confirm("Are you sure to delete customer Id: "+$scope.customer._id)==true)
            services.deleteCustomer(customer.customerNumber);
    };

    $scope.saveCustomer = function(customer) {
        //$location.path('/');
        $scope.dataLoading = true;
        if (customerID <= 0) {
            //var result = services.insertCustomer(customer);
            services.insertCustomer(customer, function(response) {
                $scope.error = response.data["msg"];
                if (response.status==200) {
                    console.log("***", response);
                    //$location.path('/');
                } else {
                    console.log("---", response, response.data["msg"]);
                    $scope.dataLoading = false;
                }
            });
        }
        else {
            services.updateCustomer(customerID, customer);
        }
    };

});

app.controller('mapCtrl', function ($scope, $rootScope, $location, $routeParams, services, customer) {
    console.log('maps', customer);
    var original = customer;
    $scope.customer = angular.copy(original);

    $scope.error = "";
    $scope.dataLoading = true;    

    $('#children').hide();
    $('#map').hide();
    $('#info').hide();  

    $scope.getgScore = function() {
        return gScore;
    };    

    $scope.getbScore = function() {
        return bScore;
    };  

    $scope.getpScore = function() {
        return pScore;
    };      

    $scope.isDirty = function() {
        return dataDirty;
    }; 

    $scope.allowUpdate = function() {
        allowUpdate = true;
        return allowUpdate;
    };

    $scope.isAllowUpdate = function() {
        return allowUpdate;
    };   
        
    $scope.saveUser = function (customer) {
        //$location.path('/');
        $scope.dataLoading = true;
        process();
        var data = {'user': customer, 'maps': mapData};
        
        console.log(">>>", data);
        customer['data'] = JSON.stringify(data, null, 4);
        customer['status'] = pScore.toString() + "," + bScore.toString() + "," + gScore.toString();
        var conf = confirm("Please make sure that the maps are filled completely.\n Ok to Submit or Cancel?")
        if (conf) {
            services.insertUser(customer, function (response) {
                $scope.error = response.data["msg"];
                if (response.status == 200) {
                    console.log("***", response);
                    //$location.path('/');
                } else {
                    console.log("---", response, response.data["msg"]);
                    
                }
            });
            $scope.dataLoading = false;
            dataDirty = false;
            allowUpdate = false;            
            $('.nav-tabs a[data-target="#map"]').show();
            $('.nav-tabs a[data-target="#map"]').tab('show');
            $('#map').show();$('#parent').hide();$('#children').hide();
            $('#info').hide();
        }
    }; 


        	
});

app.controller('familyCtrl', function ($scope, services) {
    services.getData().then(function(data){
        $scope.maps = data.data;
		$scope.list = true;
		$scope.map = false;
    });
	
	$scope.map_title = ["parent", "children-boys", "children-girls", "parent-score", "boys-score", "girls-score"];
	$scope.map_box = [
	    {elem: "parent", title: "Parent", score:""}, 
		{elem: "boys", title: "Boys", score:""}, 
		{elem:"girls", title: "Girls", score:""}, 
		{elem:"family-parent", title: "Parent Harmony", score:""}, 
		{elem:"family-boys", title: "Boys Harmony", score:""}, 
		{elem:"family-girls", title: "Girls Harmony", score:""}
		];
	
    $scope.showMap = function(mapdata, score) {
		var scores = score.split(",");
		console.log(scores);
		$scope.map_box[3].score = "Score = " + scores[0];
		$scope.map_box[4].score = "Score = " + scores[1];
		$scope.map_box[5].score = "Score = " + scores[2];
		
		init();
		setMap();
		
		//console.log(mapdata);
		$scope.list = false;
		$scope.map = true;
		allowUpdate = false;
		var map = JSON.parse(mapdata);
		console.log(map);
		$scope.user = map.user;
		mapData = map.maps;
		setMap();
		//$scope.selected_map_data = mapdata;
        return;
    };   	
});

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/login', {
                title: 'Login',
                hideMenus: true,
                templateUrl: 'partials/login.html',
                controller: 'LoginController', //'loginCtrl',
                resolve: {
                    user: function(services, $route) {
                        return {'email': 'a', 'pwd': 'b'};
                    }
                }
            })
            .when('/demo', {
                title: 'Demo',
                templateUrl: 'partials/demo.html',
                controller: 'mainCtrl'
            })
            .when('/edit-customer', {
                title: 'Edit Account',
                templateUrl: 'partials/edit-customer.html',
                controller: 'editCtrl',
                resolve: {
                    customer: function(services, $route){
                        var customerID = services.getAccountId();
                        return services.getCustomer(customerID);
                    }
                }
            })
            .when('/change-password', {
                title: 'New Password',
                templateUrl: 'partials/new-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route){ return null;}
                }
            })
            .when('/forgot-password', {
                title: 'Forgot Password',
                templateUrl: 'partials/forgot-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route){ return null;}
                }
            })
            .when('/reset-password/:email/:reset', {
                title: 'Reset Password',
                templateUrl: 'partials/new-password.html',
                controller: 'passwordCtrl',
                resolve: {
                    user: function(services, $route) {
                        var email = $route.current.params.email;
                        var reset = $route.current.params.reset;
                        //console.log(email, reset);
                        return services.resetPassword(email, reset);
                    }
                }
            })
            .when('/', {
                title: 'Family',
                templateUrl: 'partials/maps.html',
                controller: 'mapCtrl',
                resolve: {
                    customer: function(services, $route) {
                        return {age1: 35, age2: 30, boys:1, girls:1};
                    }
                }                
            })
            .when('/showmaps', {
                title: 'Family Maps',
                templateUrl: 'partials/showmaps.html',
                controller: 'showMapCtrl'
            })
            .when('/maps', {
                title: 'Family Map Data',
                templateUrl: 'partials/mapList.html',
                controller: 'familyCtrl'
            })             
            .otherwise({
                redirectTo: '/'
            });
    }]);

app.run(['$location', '$rootScope',  '$cookieStore', '$http',
    function($location, $rootScope, $cookieStore, $http) {
        //keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            //logged_id = localStorage.getItem("logged_id");
            if ($location.path() !== '/login' && $location.path() !== '/edit-customer' &&
                !($location.path().indexOf('reset-password')!=-1) &&
                $location.path() !== '/forgot-password' && $location.path() !== '/maps' && 
                $location.path() !== '/demo' && !$rootScope.globals.currentUser) {
                $location.path('/');
            }
            //$rootScope.title = current.$route.title;
        });
    }
]);
