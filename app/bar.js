
function plotBar(div, title_text, subtitle, categories, y_title, data) {

    // Build the chart
    Highcharts.chart(div, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            },
            type: 'column'
        },
        credits: {
            enabled: false
        },
        legend: {
            margin: 2, padding: 5, y:15, layout: 'horizontal', align: 'center', verticalAlign: 'bottom',
            shadow: true, backgroundColor: '#FFFFFF',
            title: {
                style: {fontStyle: 'italic', fontSize: '10px'}
            }
        },
        title: {
            text: title_text,
            margin: 20, x: -10,
            align: 'center',
            style: {color:'red', fontSize: '24px', fontFamily: 'Arial', fontStyle: 'bold'}
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: y_title,
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            //pointFormat: '{series.name}: <b>{point.y}, {point.percentage:.1f} %</b>'
            //valueSuffix: ' millions'
        },
        plotOptions: {
            column: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                    //format: '{point.percentage:.1f} %',
                    //style: {
                    //    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    //},
                    //connectorColor: 'silver'
                }
            }
        },
        series: data
    })
}

function plotColumn(div, title_text, subtitle, categories, y_title, data) {

    // Build the chart
    Highcharts.chart(div, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            },
            type: 'column'
        },
        credits: {
            enabled: false
        },
        legend: {
            margin: 2, padding: 5, y:15, layout: 'horizontal', align: 'center', verticalAlign: 'bottom',
            shadow: true, backgroundColor: '#FFFFFF',
            title: {
                style: {fontStyle: 'italic', fontSize: '10px'}
            }
        },
        title: {
            text: title_text,
            margin: 20, x: -10,
            align: 'center',
            style: {color:'red', fontSize: '24px', fontFamily: 'Arial', fontStyle: 'bold'}
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: y_title,
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.1f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                allowPointSelect: true,
                stacking: 'percent',
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                    //format: '{point.percentage:.1f} %',
                    //style: {
                    //    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    //},
                    //connectorColor: 'silver'
                }
            }
        },
        series: data
    })
}


function plotLine(div, title_text, subtitle, categories, y_title, data) {

    // Build the chart
    Highcharts.chart(div, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            /*options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            },*/
            zoomType: 'x',
            type: 'line'
        },
        credits: {
            enabled: false
        },
        legend: {
            margin: 2, padding: 5, y:15, layout: 'horizontal', align: 'center', verticalAlign: 'bottom',
            shadow: true, backgroundColor: '#FFFFFF',
            title: {
                style: {fontStyle: 'italic', fontSize: '10px'}
            }
        },
        title: {
            text: title_text,
            margin: 20, x: -10,
            align: 'center',
            style: {color:'red', fontSize: '24px', fontFamily: 'Arial', fontStyle: 'bold'}
        },
        subtitle: {
            text: subtitle
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: y_title,
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}; font-family: Arial, sans-serif; font-weight: bold">{series.name}: {point.y}</span> <br/>',
            shared: true
        },
        plotOptions: {
            line: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        },
        series: data
    })
}
