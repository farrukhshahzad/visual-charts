// This will change based on what a user picks...
var svg = false;
var force = false;

var wipeOnNewLoad = false;

var JSONLoaded = false;

$(document.body).on('click', '.dropdown-menu li', function (event) {

    var $target = $(event.currentTarget);

    $target.closest('.btn-group')
        .find('[data-bind="label"]').text($target.text())
        .end()
        .children('.dropdown-toggle').dropdown('toggle');

    return false;

});

var makeRadioDiv = function (name) {
    return '<div class="radio"><label><input type="radio" name="optradio" value="' + name + '">' + name + '</label></div>';
};

var makeDropdownDiv = function (name) {
    return '<li><a href="#">' + name + '</a></li>'
};

var changeLoadButton = function () {
    if (JSONLoaded) {
        $('#loadbutton').text('Graph Settings');
    } else {
        $('#loadbutton').text('Load JSON');

    }
};


var loadFile = function () {

    $('#filePickerModal').modal('hide');

    var input, file, fr;

    if (typeof window.FileReader !== 'function') {
        alert("The file API isn't supported on this browser yet.");
        return;
    }

    input = document.getElementById('fileinput');
    if (!input) {
        alert("Um, couldn't find the fileinput element.");
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
    }
    else {
        file = input.files[0];

        if (!file || file.type !== "application/json") {
            //Farrukh - I need to comment following 2 lines to make it work
            //alert("Pick a file, and ensure that it is type json");
            //return;
        }

        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
        JSONLoaded = true;
        //changeLoadButton();
    }

    function receivedText(e) {
        lines = e.target.result;
        var data = JSON.parse(lines);
        parseData(data);
    }
};



